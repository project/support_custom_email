----------------
Requirements:
----------------
 - Drupal 6.x
 - support.module (http://drupal.org/project/support)
 - content.module   (http://drupal.org/project/cck)

----------------
Installation:
----------------
0) Upload the module to the support folder
1) Enable the module in your installation

---------------
Usage:
---------------
0) Go to the settings page for Support Ticketing System
1) Click the new Mail settings tab
2) Change your e-mail texts

---------------
If i18nstrings:
---------------
0) First make sure you have visited the mail settings page so that the strings have been registered
1) Go to translate interface
2) Search for all strings on Support Ticketing System